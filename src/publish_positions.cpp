#include <stdio.h>
#include <stdlib.h>

#include "ros/ros.h"

#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"

#include "std_msgs/Float64MultiArray.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "positionsPublisher");
  ros::NodeHandle nh;

  std::vector<double> grippingPose;
  std::vector<double> cabinaAttachPose;
  if(!nh.getParam("/new_sim/grippingPose", grippingPose))
    ROS_ERROR("posizione grippingPose non caricata");
  if(!nh.getParam("/new_sim/cabinaAttachPose", cabinaAttachPose))
    ROS_ERROR("posizione grippingPose non caricata");
  
  
  ros::Publisher panelPub = nh.advertise<std_msgs::Float64MultiArray>("chatterPannello", 1);
  ros::Publisher cabinPub = nh.advertise<std_msgs::Float64MultiArray>("chatterCabina", 1);
  
//   while(ros::ok()){
    std_msgs::Float64MultiArray array;
    array.data.clear();
    
    for (int i = 0; i < grippingPose.size(); i++){
      array.data.push_back(grippingPose[i]);
    }
    sleep(5);
    panelPub.publish(array);
    
    
    
    array.data.clear();
    
    for (int i = 0; i < cabinaAttachPose.size(); i++){
      array.data.push_back(cabinaAttachPose[i]);
    }
    sleep(5);
    cabinPub.publish(array);
    ros::spinOnce();
    sleep(2);
//   }

  return 0;
}