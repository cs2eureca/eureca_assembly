#include <ros/ros.h>
#include <pluginlib/class_loader.h>
#include <actionlib/server/simple_action_server.h>

#include <eigen_conversions/eigen_msg.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/kinematics_base/kinematics_base.h>

#include <moveit_msgs/GetPlanningScene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>

#include <eureca_assembly/moveIiwa.h>
#include <eureca_assembly/moveIiwaAction.h>

#include <tf/transform_listener.h>
#include <gazebo_ros_link_attacher/Attach.h>
#include "geometric_shapes/shape_operations.h"
#include <gazebo_msgs/DeleteModel.h>

#include <descartes_moveit/ikfast_moveit_state_adapter.h>
#include <descartes_trajectory/axial_symmetric_pt.h>
#include <descartes_trajectory/cart_trajectory_pt.h>
#include <descartes_trajectory/joint_trajectory_pt.h>
#include <descartes_planner/dense_planner.h>
#include <descartes_utilities/ros_conversions.h>
#include <cartesian_path_utils/cartesian_path_utils.h>
#include <eigen_conversions/eigen_msg.h>

#include <moveit_planning_helper/manage_planning_scene.h>

static const char* DEFAULT      = "\033[0m";
static const char* RESET        = "\033[0m";
static const char* BLACK        = "\033[30m";
static const char* RED          = "\033[31m";
static const char* GREEN        = "\033[32m";
static const char* YELLOW       = "\033[33m";
static const char* BLUE         = "\033[34m";
static const char* MAGENTA      = "\033[35m";
static const char* CYAN         = "\033[36m";
static const char* WHITE        = "\033[37m";
static const char* BOLDBLACK    = "\033[1m\033[30m";
static const char* BOLDRED      = "\033[1m\033[31m";
static const char* BOLDGREEN    = "\033[1m\033[32m";
static const char* BOLDYELLOW   = "\033[1m\033[33m";
static const char* BOLDBLUE     = "\033[1m\033[34m";
static const char* BOLDMAGENTA  = "\033[1m\033[35m";
static const char* BOLDCYAN     = "\033[1m\033[36m";
static const char* BOLDWHITE    = "\033[1m\033[37m";


std::string PANEL_ASSEMBLY_SERVICE       =  "plan_panel_assembly"        ;
std::string LINK_ATTACHER_SRV_DEFAULT_NS =  "/link_attacher_node/attach" ;
std::string LINK_DETACHER_SRV_DEFAULT_NS =  "/link_attacher_node/detach" ;
std::string PANEL_ASSEMBLY_ACTION_DEFAULT_NS     = "panel_assembly_action";

std::map< std::string, std::string >  PLANNING_GROUP             =  { { "olivia", "manipulator"                       }, {"popeye", "manipulator"                        } };
std::map< std::string, std::string >  LINK_0                     =  { { "olivia", "olivia_link_0"                     }, {"popeye", "popeye_link_0"                      } };
std::map< std::string, std::string >  LINK_EE                    =  { { "olivia", "olivia_link_7"                     }, {"popeye", "popeye_link_ee"                     } };
std::map< std::string, std::string >  GRIP_END_EFFECTOR          =  { { "olivia", "EE_link_grip"                      }, {"popeye", "EE_link_grip"                       } };
std::map< std::string, std::string >  ATTACH_END_EFFECTOR        =  { { "olivia", "EE_link_attach"                    }, {"popeye", "EE_link_attach"                     } };
std::map< std::string, std::string >  PANEL_ID                   =  { { "olivia", "side_panel.link_0"                 }, {"popeye", "side_panel.link_0"                  } };
std::map< std::string, std::string >  ROBOT_DESCRIPTION          =  { { "olivia", "/olivia/robot_description"         }, {"popeye", "/olivia/robot_description"          } };
std::map< std::string, std::string >  ROBOT_DESCRIPTION_SEMANTIC =  { { "olivia", "/olivia/robot_description_semantic"}, {"popeye", "/olivia/robot_description_semantic" } };
std::map< std::string, std::string >  GET_PLANNING_SCENE         =  { { "olivia", "/olivia/get_planning_scene"        }, {"popeye", "/popeye/get_planning_scene"         } };


struct AttachModel 
{
  const std::string model_1;
  const std::string link_1;
  const std::string model_2;
  const std::string link_2;
  const bool        attach;
  AttachModel ( std::string m1, std::string l1, std::string m2,  std::string l2, bool a ) : model_1(m1), link_1(l1),  model_2(m2), link_2(l2), attach(a) {} 
};

std::vector<AttachModel> olivia_attach_models = { AttachModel( "panel_cart" , "link_0", "side_panel"  , "link_0", false )
                                                , AttachModel( "panel_cart" , "link_0", "ground_plane", "link"  , true  ) 
                                                , AttachModel( "olivia"     , "olivia_link_7" , "side_panel"    , "link_0" , true  ) };
                                                
std::vector<AttachModel> popeye_attach_models = { AttachModel( "panel_cart" , "link_0", "side_panel"  , "link_0", false )
                                                , AttachModel( "panel_cart" , "link_0", "ground_plane", "link"  , true  ) 
                                                , AttachModel( "popeye"     , "popeye_link_7" , "side_panel"    , "link_0" , true  ) };
std::map< std::string, std::vector<AttachModel>  > ATTACH_MODELS  = { { "olivia", olivia_attach_models }
                                                                    , { "popeye", popeye_attach_models } };
  

robot_state::RobotState getRobotState ( ros::NodeHandle&                       nh
                                      , const moveit::core::RobotModelConstPtr robot_model
                                      , const std::string&                     robot_arm_name
                                      ) 
{
  
  ros::ServiceClient planning_scene_service;
  planning_scene_service = nh.serviceClient<moveit_msgs::GetPlanningScene>(GET_PLANNING_SCENE[robot_arm_name]);
  if( !planning_scene_service.waitForExistence( ros::Duration(5.0) ) )
  {
    ROS_ERROR("getRobotState Failed: service '%s' does not exist ", GET_PLANNING_SCENE[robot_arm_name].c_str() );
  }
  
  robot_state::RobotState* ret = new robot_state::RobotState( robot_model );
  
  moveit_msgs::GetPlanningScene::Request request;
  moveit_msgs::GetPlanningScene::Response response;
    
  request.components.components = request.components.ROBOT_STATE;
  if (!planning_scene_service.call(request, response))
  {
    ROS_WARN("Could not call planning scene service to get object names");
  }

  moveit::core::robotStateMsgToRobotState(response.scene.robot_state, *ret);
  
  return *ret;
}

class assemblyPlanner
{
protected:
  ros::NodeHandle nh_;
  std::string     robot_arm_name_;
  bool                                                            initialized_;
  robot_model_loader::RobotModelLoaderPtr                         robot_model_loader_       ;
  robot_model::RobotModelPtr                                      robot_model_              ;
  moveit::planning_interface::MoveGroupInterfacePtr               move_group_               ;
  moveit::planning_interface::PlanningSceneInterfacePtr           planning_scene_interface_ ;
  actionlib::SimpleActionServer<eureca_assembly::moveIiwaAction > as_; 
  eureca_assembly::moveIiwaResult                                 result_;
  
public:
  assemblyPlanner( ros::NodeHandle nh) 
  : nh_( nh ), initialized_( false ), as_(nh_, PANEL_ASSEMBLY_ACTION_DEFAULT_NS, boost::bind(&assemblyPlanner::executeCB, this, _1), false)
  {
    as_.start();
  }
 
  moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  
  bool  attachObj( std::string model1, std::string link1, std::string model2, std::string link2, bool attach = true){
        
    ROS_INFO("[ LINK ] Waiting for server ....");
    ros::ServiceClient attachModel;
    if(attach)
      attachModel = nh_.serviceClient<gazebo_ros_link_attacher::Attach>(LINK_ATTACHER_SRV_DEFAULT_NS);
    else
      attachModel = nh_.serviceClient<gazebo_ros_link_attacher::Attach>(LINK_DETACHER_SRV_DEFAULT_NS);
    
    gazebo_ros_link_attacher::Attach srvAttach;

    attachModel.waitForExistence();

    std::string att = (attach ? "attaching" : "detaching");
    
    ROS_INFO_STREAM( att << " " << model1 << " and " << model2);
    
    srvAttach.request.model_name_1 = model1;
    srvAttach.request.link_name_1  = link1;
    srvAttach.request.model_name_2 = model2;
    srvAttach.request.link_name_2  = link2;

    ROS_INFO("[ LINK ] Wait for result...." );
    attachModel.call(srvAttach);
    if(!srvAttach.response.ok)
    {
      ROS_ERROR_STREAM("[ LINK ] Error in attach the model " << srvAttach.request );
      return false;
    }
    
    ROS_INFO_STREAM("[ LINK ] success!"<<  std::to_string( srvAttach.response.ok ).c_str() );
    
    return true;
  };
  
  bool checkCollision ( const geometry_msgs::Pose         robot_pose
                      , bool                              verbose = true )
  {
    if(verbose) ROS_INFO_STREAM("checkCollision of " << robot_pose ); 
    planning_scene::PlanningScenePtr  planning_scene( new planning_scene::PlanningScene(robot_model_) );
    
    collision_detection::CollisionResult collision_result;    
    collision_result.clear();
    
    moveit::core::RobotState check_state( robot_model_ ); 
    if( !check_state.setFromIK(robot_model_->getJointModelGroup(PLANNING_GROUP[robot_arm_name_]), robot_pose ) )
    {
      ROS_ERROR("IK failed");
      return false;
    };
    check_state.update();

//     geometry_msgs::Pose p;
//     Eigen::Affine3d     g;    
//     if(verbose) 
//     {
//       ROS_WARN(" =================================================" );
//       std::cout << robot_model_->getModelFrame() << std::endl;
//     for( auto  const & c : robot_model_->getJointModelGroup(PLANNING_GROUP[robot_arm_name_])->getLinkModelNames() )
//     {
//       std::cout << c << std::endl;
//       g = check_state.getFrameTransform( c );
//       tf::poseEigenToMsg(g,p);
//       std::cout << c << " - getFrameTransform - " << std::endl << p << std::endl;
//       g = check_state.getGlobalLinkTransform( c );  tf::poseEigenToMsg(g,p);
//       std::cout << c << " - getGlobalLinkTransform - " << std::endl << p << std::endl;
// 
//     }
//     ROS_WARN(" =================================================" );

//    moveit_planning_helper::RobotStateExtended rob_state( nh_, robot_model_, PLANNING_GROUP[robot_arm_name_], GRIP_END_EFFECTOR[robot_arm_name_], "/" + robot_arm_name_ ); 
//    std::vector<double> q; check_state.copyJointGroupPositions(PLANNING_GROUP[robot_arm_name_], q);
//    rob_state.setRobotState(q, false, true );
//    std::vector<robot_state::RobotState> rss = rob_state.getRobotStateConfigurations();
//    for( auto const &  rs : rss )
//    {
//      moveit_msgs::RobotState robot_state_msg;
//      moveit::core::robotStateToRobotStateMsg( rs, robot_state_msg) ;
//      std::cout << "- " <<  robot_state_msg << std::endl; 
//    }
   
    collision_detection::CollisionRequest collision_request;    
    collision_request.contacts = true;
    collision_request.max_contacts = 1000;
    planning_scene->checkCollision(collision_request, collision_result, check_state, planning_scene->getAllowedCollisionMatrix());
    
    if(collision_result.collision )
    {
      ROS_ERROR(" In collision " );
      collision_detection::CollisionResult::ContactMap::const_iterator it;
      for(it = collision_result.contacts.begin(); it != collision_result.contacts.end(); ++it)    
        ROS_INFO(" - Contact between: %s and %s", it->first.first.c_str(), it->first.second.c_str());
    }
    else if ( verbose )
      ROS_INFO("Not in collision " );

    return collision_result.collision == 0;
  }
  
  bool planAndMove(geometry_msgs::Pose pose, std::string ee, bool PoseTarget = false)
  { 
    ROS_INFO("%s <<<< PLAN AND MOVE ", BOLDYELLOW);
    ROS_INFO("%s ---- check collision", BOLDYELLOW);
    if( !checkCollision( pose ) )
    {
      ROS_ERROR("%s >>>> the goal is in collision", BOLDYELLOW);
      return false;
    }
    
    move_group_->setStartStateToCurrentState();
    move_group_->setPoseTarget(pose,ee);
       
    ROS_INFO("%s ---- planning", BOLDYELLOW);
    if(!(move_group_->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS))
    {
      ROS_ERROR("%s >>>> PLAN AND MOVE, ERROR in Planning", BOLDYELLOW);
      return false;
    }
    
    ROS_INFO("%s ---- Ok, Execute action start", BOLDYELLOW);
    robot_state::RobotState rs = getRobotState ( nh_, robot_model_, robot_arm_name_ ); 
    
    
    if( !move_group_->execute(my_plan) )
    {
      ROS_INFO("%s --- MOVE, FAILED", RED);
      return false;
    }
    
    
    ROS_INFO("%s >>> PLAN AND MOVE, OK!!!", BOLDGREEN);
    return true;
  }
  
  void init( const std::string&  robot_arm_name)
  {
    if(initialized_ )
        return;
      
    robot_arm_name_ = robot_arm_name;
    
    std::string str;
    do
    {
      ros::Duration(0.5).sleep();
      ROS_INFO("Waiting for moveit config %s...", ROBOT_DESCRIPTION_SEMANTIC[robot_arm_name_].c_str() );
    }
    while( ! nh_.getParam(ROBOT_DESCRIPTION_SEMANTIC[robot_arm_name_], str ) );
    
    robot_model_loader_       . reset( new robot_model_loader::RobotModelLoader(ROBOT_DESCRIPTION[robot_arm_name_]) );
    robot_model_              = robot_model_loader_->getModel();
    move_group_               .reset( new moveit::planning_interface::MoveGroupInterface(PLANNING_GROUP[robot_arm_name_]) );
    planning_scene_interface_. reset( new moveit::planning_interface::PlanningSceneInterface  () );
    
    move_group_->setPlanningTime(15.0);
    initialized_ = true; 
  }
  
  void executeCB(const eureca_assembly::moveIiwaGoalConstPtr& goal)
  {
    ROS_INFO("%s\n\n =============================== NEW ASSEMBLY ACTION RECEIVED ====================================\n\n ", BOLDMAGENTA );
    
    init( goal->robot_arm_name );
        
    //-- POSITION DEFINITION -------------------------
    std::vector< std::string > objs_id { PANEL_ID[robot_arm_name_] }; 
    ROS_INFO("getting the objects scene");
    std::map< std::string, geometry_msgs::Pose > objs = planning_scene_interface_->getObjectPoses (objs_id);
    
    for( auto const & obj : objs )
      std::cout << "-" << obj.first << ":" << obj.second << std::endl;
    
    geometry_msgs::Pose grippingPose = objs[PANEL_ID[robot_arm_name_]];
    geometry_msgs::Pose descartPose  = grippingPose;
                        descartPose.position.x += 0.0;
                        descartPose.position.z += 0.1;
                        
    geometry_msgs::Pose attachPose = goal->attach_pose;
    //-- END POSITION DEFINITION -------------------------
      
    // Approach
    ROS_INFO("%s*************** Move to the gripping pose ***************************", BOLDCYAN);
    std::cout << grippingPose << std::endl;
    if( !planAndMove(grippingPose, GRIP_END_EFFECTOR[robot_arm_name_],true) )
    {
      ROS_ERROR_STREAM("PlanAndMove to GRIPPING POSE failed" );
      result_.success= 0;
      as_.setSucceeded(result_);
      return;
    }
    // End First Movement
    
    ROS_INFO("%s*************** Link/unLink ***************************", BOLDCYAN);
    // Link/unlink
    for( AttachModel const & attach_model : ATTACH_MODELS[robot_arm_name_] )
    {    
      ROS_INFO("%s %slink '%s' and '%s'", BOLDGREEN, (attach_model.attach ? "" : "un"), attach_model.model_1.c_str(), attach_model.model_2.c_str() );
      
      if (!attachObj(attach_model.model_1, attach_model.link_1, attach_model.model_2, attach_model.link_2, attach_model.attach ))
      {
        ROS_ERROR("unable to %slink '%s' and '%s'", (attach_model.attach ? "" : "un"), attach_model.model_1.c_str(), attach_model.model_2.c_str() );
      }
    }
    
    ROS_INFO("attaching side_panel to gripper");
    if( !move_group_->attachObject("side_panel.link_0",GRIP_END_EFFECTOR[robot_arm_name_]))
    {
      ROS_WARN("failed in attaching side_panel to gripper");
    }
    
    // Second Movement
    ROS_INFO("%s*************** Move out ***************************", BOLDCYAN);
    std::cout << descartPose << std::endl;
    if( !planAndMove(descartPose, GRIP_END_EFFECTOR[robot_arm_name_],true) )
    {
      ROS_ERROR_STREAM("PlanAndMove to wayout point failed" );
      result_.success= 0;
      as_.setSucceeded(result_);
      return;
    }
    // End Second Movement
   
   
    ROS_INFO("%s*************** Move to assembly position ***************************", BOLDCYAN);
    std::cout << attachPose << std::endl;
    if( !planAndMove(attachPose, GRIP_END_EFFECTOR[robot_arm_name_],true) )
    {
      ROS_ERROR_STREAM("PlanAndMove to assembly position failed" );
      result_.success= 0;
      as_.setSucceeded(result_);
      return;
    }
    
    result_.success= 1;
    as_.setSucceeded(result_);
    return;    
  }
  
};


int main(int argc, char** argv)
{
  ros::init(argc, argv, PANEL_ASSEMBLY_SERVICE);
  ros::NodeHandle nh("~");
  ros::AsyncSpinner spinner(8);
  spinner.start();
  
  assemblyPlanner planner( nh );

  ros::waitForShutdown();

  return 0;
}
