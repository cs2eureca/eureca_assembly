
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/robot_state/conversions.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

#include <string> 
#include <iostream>
#include <algorithm>
#include <cstdlib> 
#include <ctime>
#include "geometric_shapes/shape_operations.h"

#include "std_msgs/String.h"
#include "std_msgs/Float64MultiArray.h"
// #include "individuo.h"

using namespace std;

std::vector<double> theta_0;
moveit_msgs::RobotTrajectory traj;
double driftIndex;
double planningTime;
moveit::planning_interface::MoveGroupInterface::Plan my_plan;
static const double pi = 3.1415926535;

geometry_msgs::Pose getpose(tf::Vector3 xyz, tf::Quaternion wxyz){
  
  geometry_msgs::Pose targetPose;
  
  targetPose.position.x = xyz.getX();
  targetPose.position.y = xyz.getY();
  targetPose.position.z = xyz.getZ();
  
  targetPose.orientation.x = wxyz.getX();
  targetPose.orientation.y = wxyz.getY();
  targetPose.orientation.z = wxyz.getZ();
  targetPose.orientation.w = wxyz.getW();
  
  return targetPose;
}
void printPoseWarn(tf::Vector3 origin, tf::Quaternion rotation){
  
  ROS_WARN("position x = %f", origin.getX());
  ROS_WARN("position y = %f", origin.getY());
  ROS_WARN("position z = %f", origin.getZ());
  
  ROS_WARN("rotation w= %f", rotation.getW());
  ROS_WARN("rotation x= %f", rotation.getX());
  ROS_WARN("rotation y= %f", rotation.getY());
  ROS_WARN("rotation z= %f", rotation.getZ());
}
void printPoseWarn(tf::Transform pose){
  
  ROS_WARN("position x = %f", pose.getOrigin().getX());
  ROS_WARN("position y = %f", pose.getOrigin().getY());
  ROS_WARN("position z = %f", pose.getOrigin().getZ());
  
  ROS_WARN("rotation w= %f", pose.getRotation().getW());
  ROS_WARN("rotation x= %f", pose.getRotation().getX());
  ROS_WARN("rotation y= %f", pose.getRotation().getY());
  ROS_WARN("rotation z= %f", pose.getRotation().getZ());
}
void printInfo(int poseCount, double driftIndex, moveit::planning_interface::MoveGroupInterface::Plan *my_plan){
		cout << "Pose "<<poseCount<<" planned and executed\n";
		cout << "drift Index = " << to_string(driftIndex) << "\n";
		cout << "tempo di pianificazione " << to_string(my_plan->planning_time_) << "\n";
}
void poseDefinition(std::vector<double> *pose ,tf::Transform *transform){
  
  tf::Vector3 v = tf::Vector3(pose->at(0),pose->at(1),pose->at(2));
	tf::Quaternion q;
		
	if(pose->size() == 6)
		q = tf::createQuaternionFromRPY(pose->at(3),pose->at(4),pose->at(5)); 
	else
		q = tf::Quaternion(pose->at(4),pose->at(5),pose->at(6),pose->at(3)); 
	
  transform->setOrigin(v);
  transform->setRotation(q);	
}

void addObject(string collisionObjID, string planningFrame, 
		tf::Transform *pose, moveit_msgs::CollisionObject *collision_object, 
		Eigen::Vector3d b={1.0, 1.0, 1.0},
		string stlID = ""){

  if (stlID == "")
    stlID = collisionObjID;

  collision_object->id = collisionObjID;
  shapes::Mesh* m = shapes::createMeshFromResource("package://sim_iiwa_description/stl/"+stlID, b);

  shape_msgs::Mesh mesh;
  shapes::ShapeMsg mesh_msg;
  shapes::constructMsgFromShape(m, mesh_msg);
  mesh = boost::get<shape_msgs::Mesh>(mesh_msg);
  
  collision_object->meshes.resize(1);
  collision_object->mesh_poses.resize(1);
  collision_object->meshes[0] = mesh;
  collision_object->header.frame_id = planningFrame;
  
  tf::Vector3 origin = pose->getOrigin();
  tf::Quaternion rotation = pose->getRotation();
  
  geometry_msgs::Pose Pose;
  Pose = getpose(origin,rotation);
  
  collision_object->mesh_poses[0] = Pose;
	  
  collision_object->meshes.push_back(mesh);
  collision_object->mesh_poses.push_back(collision_object->mesh_poses[0]);
  collision_object->operation = collision_object->ADD;
}

std::vector<double> differenza(std::vector<double>  theta_0, std::vector<double>  theta, int size){
	std::vector<double>  result(size);
// 	theta->size());
	for(int i = 0; i < size; i++){
		result[i] = theta[i]-theta_0[i];
	}
	return result;
}
double moltiplicazione(std::vector<double>  diff){
	double  result;
	for(int i = 0; i < diff.size(); i++){
		result += diff[i]*diff[i];
	}
	return result;
}
double computeDriftIndex(std::vector<double> theta_0, moveit_msgs::RobotTrajectory traj){

	int size = theta_0.size();
	std::vector<double>  theta(size);
	std::vector<double>  diff(size);
	double molt;
	double result = 0;

	for(int i = 0; i < traj.joint_trajectory.points.size(); i++){
		for(int j = 0; j < size; j++){
			theta[j] = traj.joint_trajectory.points[i].positions[j];
		}
		diff = differenza(theta_0,theta, size);
		molt = moltiplicazione(diff);
		result += molt/2;
	}
	return result;
}

bool planAndMove(moveit::planning_interface::MoveGroupInterface *move_group, 
								 geometry_msgs::Pose targetPose, const string endEffector){
			
		move_group->setPoseTarget(targetPose,endEffector);
		
		if(!(move_group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS))
			return false;
		
		theta_0 = move_group->getCurrentJointValues();
		traj = my_plan.trajectory_;
		
		planningTime += my_plan.planning_time_;
		driftIndex += computeDriftIndex(theta_0, traj);
		
		move_group->execute(my_plan);
		
		return true;
}
bool planAndMove(moveit::planning_interface::MoveGroupInterface *move_group, 
									std::vector<double> jointTargetPose){
			
		move_group->setJointValueTarget(jointTargetPose);
		
		if(!(move_group->plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS))
			return false;
		
		theta_0 = move_group->getCurrentJointValues();
		traj = my_plan.trajectory_;
		
		planningTime += my_plan.planning_time_;
		driftIndex += computeDriftIndex(theta_0, traj);
		
		move_group->execute(my_plan);
		
		return true;
}

void noSuccess(moveit::planning_interface::MoveGroupInterface *move_group,
							 moveit_msgs::CollisionObject *collision_object,
							 moveit::planning_interface::PlanningSceneInterface *planning_scene_interface,
							 std::vector<std::string> *object_ids
							){
	ROS_ERROR("pianificazione non riuscita");
	move_group->detachObject(collision_object->id);
	ros::Duration(1.0).sleep();
	planning_scene_interface->removeCollisionObjects(*object_ids);
}

double roll,pitch,yaw;

void toEulerAngle(tf::Quaternion q, double *roll, double *pitch, double *yaw)
{
	// roll (x-axis rotation)
	double sinr = 2 * (q.getW() * q.getX() + q.getY() * q.getZ());
	double cosr = 1 - 2 * (q.getX() * q.getX() + q.getY() * q.getY());
	*roll = atan2(sinr, cosr);
	// pitch (y-axis rotation)
	double sinp = 2 * (q.getW() * q.getY() - q.getZ() * q.getX());
	if (fabs(sinp) >= 1)
		*pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		*pitch = asin(sinp);
	// yaw (z-axis rotation)
	double siny = 2 * (q.getW() * q.getZ() + q.getX() * q.getY());
	double cosy = 1 - 2 * (q.getY() * q.getY() + q.getZ() * q.getZ());  
	*yaw = atan2(siny, cosy);
}


std::vector<double> grippingPose;
std::vector<double> cabinaAttachPose;
bool Ricevuta = false;
void setGrippingPose(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  cout<<"grippingpose \n";
  for (int i=0; i<msg->data.size();i++){
    grippingPose.push_back(msg->data[i]);
//     cout<<", "<<to_string(grippingPose[i]);
  }
  Ricevuta = true;
}

void setAttachPose(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  cout<<"attach pose \n";
  for (int i=0; i<msg->data.size();i++){
    cabinaAttachPose.push_back(msg->data[i]);
//     cout<<", "<<to_string(cabinaAttachPose[i]);
  }
  Ricevuta = true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "iiwa14");
  ros::NodeHandle nh;
  ros::AsyncSpinner spinner(4);
  spinner.start();
  srand(time(NULL));
	
  static const string PLANNING_GROUP = "olivia";
  const string endEffector = "Gripper_ee_link";
  
  int poseCount;
  bool success;
  
  moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  
  geometry_msgs::PoseStamped currentPose;
  geometry_msgs::PoseStamped orient;
  
  move_group.setPlanningTime(15.0);

  std::vector<double> jointHome;
  if(!nh.getParam("/new_sim_fixed_positions/jointHome", jointHome))
  { ROS_ERROR("posizione jointHome non caricata");
    return -1;
  }
  std::vector<double> rotationAttachPose;
  if(!nh.getParam("/new_sim_fixed_positions/rotationAttachPose", rotationAttachPose))
	{ ROS_ERROR("posizione rotationAttachPose non caricata");
    return -1;
  }
  std::vector<double> robotOrigin;
  if(!nh.getParam("/new_sim_fixed_positions/robotOrigin", robotOrigin))
  { ROS_ERROR("posizione origine non caricata");
    return -1;
  }
  std::vector<double> preAttachPoint;
  if(!nh.getParam("/new_sim_fixed_positions/preAttachPoint", preAttachPoint))
  { ROS_ERROR("posizione preAttachPoint non caricata");
    return -1;
  }
  std::vector<double> preAttachPointRot;
  if(!nh.getParam("/new_sim_fixed_positions/preAttachPointRot", preAttachPointRot))
  { ROS_ERROR("posizione preAttachPointRot non caricata");
    return -1;
  }
  std::vector<double> gripPoint;
  if(!nh.getParam("/new_sim_fixed_positions/gripPoint", gripPoint))
  { ROS_ERROR("posizione gripPoint non caricata");
    return -1;
  }
  std::vector<double> secondPanel;
  if(!nh.getParam("/new_sim_fixed_positions/secondPanel", secondPanel))
  { ROS_ERROR("posizione secondPanel non caricata");
    return -1;
  }
  std::vector<double> panelLean;
  if(!nh.getParam("/new_sim_fixed_positions/panelLean", panelLean))
  { ROS_ERROR("posizione panelLean non caricata");
    return -1;
  }
  std::vector<double> intermediatePose;
  if(!nh.getParam("/new_sim_fixed_positions/intermediatePose", intermediatePose))
  { ROS_ERROR("posizione intermediatePose non caricata");
    return -1;
  }
  std::vector<double> rotationGrippingPose;
  if(!nh.getParam("/new_sim_fixed_positions/rotationGrippingPose", rotationGrippingPose))
  { ROS_ERROR("posizione rotationGrippingPose non caricata");
    return -1;
  }
  std::vector<double> cabPose;
  if(!nh.getParam("/new_sim_fixed_positions/cabPose", cabPose))
  { ROS_ERROR("posizione cabPose non caricata");
    return -1;
  }
  std::vector<double> cabTrasl;
  if(!nh.getParam("/new_sim_fixed_positions/cabTrasl", cabTrasl))
  { ROS_ERROR("posizione cabTrasl non caricata");
    return -1;
  }
  std::vector<double> sideCab;
  if(!nh.getParam("/new_sim_fixed_positions/sideCab", sideCab))
  { ROS_ERROR("posizione sideCab non caricata");
    return -1;
  }
  std::vector<double> panelToCartRot;
  if(!nh.getParam("/new_sim_fixed_positions/panelToCartRot", panelToCartRot))
  { ROS_ERROR("posizione panelToCartRot non caricata");
  }
  std::vector<double> panelToCartTrasl;
  if(!nh.getParam("/new_sim_fixed_positions/panelToCartTrasl", panelToCartTrasl))
  { ROS_ERROR("posizione panelToCartTrasl non caricata");
  }

//-------------------------------------- metto il robot in posizione iniziale ---------------------------------
  
  robot_model::RobotModelConstPtr robot_model( (new robot_model_loader::RobotModelLoader( "robot_description" ))->getModel() );

  currentPose = move_group.getCurrentPose(endEffector);

//--------------------------------------key poses definition----------------------------------------------------

	// trasformazione da origine del mondo a origine del robot
	
  tf::Transform worldToRobot;
  poseDefinition(&robotOrigin,&worldToRobot);

  
  tf::Transform panelToCartOriginTrasl;
  poseDefinition(&panelToCartTrasl,&panelToCartOriginTrasl);
  tf::Transform panelToCartOriginRot;
  poseDefinition(&panelToCartRot,&panelToCartOriginRot);
  // trasformazione da origine della cabina la punto di aggancio pannello
  tf::Transform CabinaToPreAttach;
  poseDefinition(&preAttachPoint,&CabinaToPreAttach);
  tf::Transform CabinaToPreAttachRot;
  poseDefinition(&preAttachPointRot,&CabinaToPreAttachRot);
  
  //  trasformazione da punto di afferraggio pannello a origine del pannello
  
  tf::Transform gripPointToPanelOrigin;
  poseDefinition(&gripPoint,&gripPointToPanelOrigin);
  tf::Transform secondPanelOrigin;
  poseDefinition(&secondPanel,&secondPanelOrigin);
  
  tf::Transform panelLeanGrip;
  poseDefinition(&panelLean,&panelLeanGrip);
  
// 	rotazione del gripper
  
  tf::Transform gripperRotation;
  poseDefinition(&rotationGrippingPose,&gripperRotation);
  
  // posizione dell' origine della cabina 
  
  tf::Transform attachPointToCabinaOrigin;
  poseDefinition(&cabPose,&attachPointToCabinaOrigin);
  
  tf::Transform traslAttachPointToCabinaOrigin;
  poseDefinition(&cabTrasl,&traslAttachPointToCabinaOrigin);
  
  tf::Transform sideCabinaOrigin;
  poseDefinition(&sideCab,&sideCabinaOrigin);
  
  // posa del robot all aggancio del pannello
  
  tf::Transform endEffectorPose4Rotation;
  poseDefinition(&rotationAttachPose,&endEffectorPose4Rotation);
  
// 	 intermezzo sopra alla capaattachPointToCabinaOrigin
  
  tf::Transform middlePose;
  poseDefinition(&intermediatePose,&middlePose);
  
  tf::Transform intermediate = worldToRobot * middlePose;
  
  tf::Vector3 originInt = intermediate.getOrigin();
  tf::Quaternion rotationInt = intermediate.getRotation();
  
	
//----------------------------------------------inizio a muovere-------------------------------------

  move_group.setStartStateToCurrentState();
//   planAndMove(&move_group, jointHome);

  currentPose = move_group.getCurrentPose(endEffector);
  cout<<"posa corrente!!!!!!!!1\n\n\n\n";
  cout<<to_string(currentPose.pose.position.x)+" ";
  cout<<to_string(currentPose.pose.position.y)+" ";
  cout<<to_string(currentPose.pose.position.z)+" ";
  cout<<to_string(currentPose.pose.orientation.w)+" ";
  cout<<to_string(currentPose.pose.orientation.x)+" ";
  cout<<to_string(currentPose.pose.orientation.y)+" ";
  cout<<to_string(currentPose.pose.orientation.z)+" ";
  
  cout<<"posa corrente!!!!!!!!1\n\n\n\n";
  
  geometry_msgs::Pose targetPose;
  
  planningTime = 0;
  driftIndex = 0;
  poseCount = 1;
  success = true;
	
//-------------------------------aspetto pubblicazione posizione pannello---------------------------------------------- 

  ros::Subscriber sub;
  cout<<"aspetto pubblicazione pannello \n";
  while(Ricevuta == false){
    sub = nh.subscribe("chatterPannello", 1, setGrippingPose);
    ros::spinOnce();
  }
  cout<<"grippingpose: "<<to_string(grippingPose[0]);
  for (int i =1; i<grippingPose.size(); i++){
    cout<<", "<<to_string(grippingPose[i]);
  }
  
  tf::Transform endEffectorGrippingPose;
  poseDefinition(&grippingPose,&endEffectorGrippingPose);
  
  tf::Transform gripPose;
  gripPose = worldToRobot * endEffectorGrippingPose * gripperRotation;

//-------------------------------add panel to planning scene---------------------------------------------- 
  string spAdd = (argc > 2 ? argv[2] : "");
  cout << "parametro per aggiungere il pannello: " << spAdd <<"\n";
  
  string planningFrame = move_group.getPlanningFrame();
  
  string collisionObjID = "CALITO_sidePanel.stl";
  
  tf::Transform panelPose ;
  panelPose = worldToRobot * endEffectorGrippingPose * gripPointToPanelOrigin * panelLeanGrip ;
	  
  moveit_msgs::CollisionObject collision_object_panel;
  Eigen::Vector3d scalePan={0.001, 0.001, 0.001};

//if ((spAdd != "nopan"))
  addObject(collisionObjID, planningFrame, &panelPose, &collision_object_panel,scalePan);

//   string collisionObj2panID = "CALITO_sidePanel2.stl";
//   moveit_msgs::CollisionObject collision_object_panel2;
//   tf::Transform secondPanelPose;
//   secondPanelPose = panelPose * secondPanelOrigin;
//   
//   addObject(collisionObj2panID, planningFrame, &secondPanelPose , &collision_object_panel2,scalePan,collisionObjID);

//-------------------------------add cart to planning scene---------------------------------------------- 
  
  collisionObjID = "carrelloKUKA.stl";
  
  tf::Transform cartPose ;
  cartPose = worldToRobot * endEffectorGrippingPose * gripPointToPanelOrigin * panelToCartOriginTrasl * panelToCartOriginRot;
	  
  moveit_msgs::CollisionObject collision_object_cart;
  Eigen::Vector3d scaleCart={0.001, 0.001, 0.001};
  addObject(collisionObjID, planningFrame, &cartPose, &collision_object_cart,scaleCart);
  

//-------------------------------aspetto pubblicazione posizione cabina---------------------------------------------- 
//   cin.get();
  
  Ricevuta = false;
  ros::Subscriber subCabina;
  cout<<"aspetto pubblicazione cabina\n";
  while(Ricevuta == false){
    subCabina = nh.subscribe("chatterCabina", 1, setAttachPose);
    ros::spinOnce();
  }
  cout<<"cabinaAttachPose: "<<to_string(cabinaAttachPose[0]);
  for (int i =1; i<cabinaAttachPose.size(); i++){
    cout<<", "<<to_string(cabinaAttachPose[i]);
  }
  
  
  tf::Transform endEffectorAttachPose;
  poseDefinition(&cabinaAttachPose,&endEffectorAttachPose);
  
  tf::Transform robotPose3 = worldToRobot * endEffectorAttachPose * CabinaToPreAttachRot * CabinaToPreAttach;
  
  tf::Vector3 origin3 = robotPose3.getOrigin();
  tf::Quaternion rotation3 = robotPose3.getRotation();
  
  cout<<"pose 3 \n";
  printPoseWarn(robotPose3);
  
// 	 aggancio
  tf::Transform robotPose4 = worldToRobot * endEffectorAttachPose;
  
  tf::Vector3 origin4 = robotPose4.getOrigin();
  tf::Quaternion rotation4 = robotPose4.getRotation();
  
  cout<<"pose 4 \n";
  printPoseWarn(robotPose4);
    
//-------------------------------add cabina to planning scene---------------------------------------------- 
  
  
  string ohAdd = (argc > 1 ? argv[1] : "");
  cout << "parametro per aggiungere la cappelliera: " << ohAdd <<"\n";
	  
  moveit_msgs::CollisionObject collision_object_cabina;
  Eigen::Vector3d scaleFus={0.001, 0.001, 0.001};
  collisionObjID = "CALITO_newfus.stl";
  
  tf::Transform cabinaPose ;
  cabinaPose = worldToRobot * endEffectorAttachPose * attachPointToCabinaOrigin * traslAttachPointToCabinaOrigin;

  printPoseWarn(cabinaPose);
  
// 	if ((ohAdd != "nocab"))
    addObject(collisionObjID, planningFrame, &cabinaPose, &collision_object_cabina, scaleFus);
	  
  moveit_msgs::CollisionObject collision_object_sideCabina;
  collisionObjID = "CALITO_sidefus.stl";
  
  
  tf::Transform sideCabinaPose ;
  sideCabinaPose = worldToRobot * endEffectorAttachPose * attachPointToCabinaOrigin * traslAttachPointToCabinaOrigin * sideCabinaOrigin;

  printPoseWarn(sideCabinaPose);
  
  if (!(ohAdd == "nocab"))
    addObject(collisionObjID, planningFrame, &sideCabinaPose, &collision_object_sideCabina, scaleFus);
  

//  add objects to planning scene
  
  std::vector<moveit_msgs::CollisionObject> collision_vector;
//if (!(spAdd == "nopan")){
//     collision_vector.push_back(collision_object_panel);
//     collision_vector.push_back(collision_object_cart);
//}
//if (!(ohAdd == "nocab")){
// //     collision_vector.push_back(collision_object_cabina);
// //     collision_vector.push_back(collision_object_sideCabina);
//}
  planning_scene_interface.addCollisionObjects(collision_vector);
  
  ros::Duration(1.0).sleep();
  
  std::vector<std::string> object_ids;
//if (!(spAdd == "nopan")){
    object_ids.push_back(collision_object_panel.id);
    object_ids.push_back(collision_object_cart.id);
//}
//if (!(ohAdd == "nocab")){
    object_ids.push_back(collision_object_cabina.id);
    object_ids.push_back(collision_object_sideCabina.id);
//}

//---------------------------set position and orientation grip------------------------------------

  move_group.setStartStateToCurrentState();
  
  cout<<"gripping pose target\n\n\n\n\n\n";
  printPoseWarn(gripPose);
  
  cout<<"gripping pose target\n\n\n\n\n\n";
  targetPose = getpose(gripPose.getOrigin(),gripPose.getRotation());
  
  if(!(planAndMove(&move_group, targetPose, endEffector))){
//     noSuccess(&move_group,&collision_object_panel,&planning_scene_interface,&object_ids);
//     planAndMove(&move_group, jointHome);
    cout<<"non e andata bene\n";
    cin.get();
    
  }
  printInfo(poseCount, driftIndex, &my_plan);

  poseCount++;
  
  move_group.attachObject(collision_object_panel.id,endEffector);

//-------------------------------set position and orientation 3----------------------------------------
  
  move_group.setStartStateToCurrentState();
  
    cin.get();
  targetPose = getpose(origin3,rotation3);
  
  if(!(planAndMove(&move_group, targetPose, endEffector))){
    noSuccess(&move_group,&collision_object_panel,&planning_scene_interface,&object_ids);
    planAndMove(&move_group, jointHome);
    return -1;
  }
  
  printInfo(poseCount, driftIndex, &my_plan);
  
  poseCount++;
  
//-------------------------------Cartesian path--------------------------------
						
  currentPose = move_group.getCurrentPose(endEffector);
					      
  double deltaX = robotPose4.getOrigin().getX() - currentPose.pose.position.x;
  double deltaY = robotPose4.getOrigin().getY() - currentPose.pose.position.y;
  double deltaZ = robotPose4.getOrigin().getZ() - currentPose.pose.position.z;
  
  double deltaRw = robotPose4.getRotation().getW() - currentPose.pose.orientation.w;
  double deltaRx = robotPose4.getRotation().getX() - currentPose.pose.orientation.x;
  double deltaRy = robotPose4.getRotation().getY() - currentPose.pose.orientation.y;
  double deltaRz = robotPose4.getRotation().getZ() - currentPose.pose.orientation.z;
  
  std::vector<geometry_msgs::Pose> waypoints;
  geometry_msgs::Pose target_pose3 = currentPose.pose;
  
//   waypoints.push_back(target_pose3);
  target_pose3.position.x += deltaX;
  target_pose3.position.y += deltaY;
  target_pose3.position.z += deltaZ;
  
  target_pose3.orientation.w += deltaRw;
  target_pose3.orientation.x += deltaRx;
  target_pose3.orientation.y += deltaRy;
  target_pose3.orientation.z += deltaRz;
  
  waypoints.push_back(target_pose3);  // up
  
  moveit_msgs::RobotTrajectory cartesianTrajectory;
  const double jump_threshold = 0.0;
  const double eef_step = 0.01;
  double fraction = move_group.computeCartesianPath(waypoints, eef_step, jump_threshold, cartesianTrajectory);
  ROS_WARN("trajectory fraction %f",fraction);
  
  if(fraction < 0.95){
    noSuccess(&move_group,&collision_object_panel,&planning_scene_interface,&object_ids);
    cout << "traietoria non finita, posizione raggiungibile?";
    planAndMove(&move_group, jointHome);
  }
  
  moveit::planning_interface::MoveGroupInterface::Plan cartesian_plan;
  cartesian_plan.trajectory_ = cartesianTrajectory;
  move_group.execute(cartesian_plan);

  move_group.detachObject(collision_object_panel.id);
  ros::Duration(1.0).sleep();
	
//-------------------------------return to home position----------------------------------------
  
  move_group.setStartStateToCurrentState();
  
  if(!(planAndMove(&move_group, jointHome))){
    noSuccess(&move_group,&collision_object_panel,&planning_scene_interface,&object_ids);
    planAndMove(&move_group, jointHome);
  }

  return 0;
}
